create or replace package uf96 is
  /** babyte**/
  --------------------------------------------------------------------------
  -- Subject    : User functions for Sipas webservice layer
  -- File       : $Release: @releaseVersion@ $
  --              $Id: UF96.pls 98337 2018-12-13 15:05:14Z nte $
  -- Copyright (c) TIA Technology A/S. All rights reserved
  --------------------------------------------------------------------------
  /** bobute nusipirko dvirati**/
  -- Function returns 1 if product line should be included in return to SIPAS.
  -- Returns 0 if product line should be skipped
  function get_all_product_lines(p_prod_id                 varchar2,
                                 p_product_line_id         varchar2,
                                 p_product_line_version_no number) return integer;

  -- Function returns 1 if object column should be included in return to SIPAS.
  -- Returns 0 if object column should be skipped
  function get_obj_config(p_item_name      varchar2,
                          p_object_type_id varchar2) return integer;

  -- Function returns 1 if sub-object column should be included in return to SIPAS.
  -- Returns 0 if sub-object column should be skipped
  function get_obj_slave_config(p_item_name         varchar2,
                                p_obj_slave_type_id varchar2) return integer;
  /** Test **/
  -- Function returnw 1 if clauses should be included in return to SIPAS.
  -- Returns 0 if clause should be skipped
  function get_pl_clauses(p_clause_id       number,
                          p_product_line_id varchar2) return integer;

  -- Function returns 1 if lov should be included in return to SIPAS.
  -- Returns 0 if lov should be skipped
  function get_lov_config(p_table_name varchar2) return integer;

  -- Function returns 1 if policy line should be included in return to SIPAS.
  -- Returns 0 if it should be skipped
  function get_cancel_code_config(p_line_cancel_code number,
                                  p_policy_status    varchar2) return integer;

  -- Function returns 1 if lov's for specified product variants should be included in return to SIPAS.
  function get_lov_pv_config(p_product_line_id varchar2) return integer;

  -- Function returns 1 if risk column should be included in return to SIPAS.
  -- Returns 0 if object column should be skipped
  function get_risk_config(p_product_line_id     varchar2,
                           p_product_line_ver_no number,
                           p_object_type_id      varchar2,
                           p_risk_no             number) return integer;

  -- Procedure that allows to manipulate data before inserting into interested party in object table
  procedure before_insert_int_party_obj(w_id_no number);

  -- Procedure that allows to adjust policy structure received from incoming JSON document.
  procedure before_pol_trans(p_policy_table in out bis_ws.t_policy_tab);

  -- Procedure that allows post actions after policy transaction is performed.
  procedure after_pol_trans(p_policy_table in out bis_ws.t_policy_tab,
                            p_policy_no    policy.policy_no%type default null);

  /*
  uf96.post_transform_json_to_policy replaces the uf96.get_transaction_type
  It was necessary to refactor code, because it was realized that single
  json file can carry data that should end up on different policies in tia.
  And these different policies might have different transaction types
  performed on them, for example Q and P.

  The policy_table parameters data type have been ammended with field policy_trans_type
  Its being filled with value from json document: ROOT[].type. Possible values are listed
  in program resolve_transcation_code. uf96.post_transform_json_to_policy can be used
  to change original value of policy_trans_type and change the action performed on policy.

  The actual policy creation/adjustement logic where policy_trans_type is used is
  happening on bis_policy.iniatiate_transaction
  */
  procedure post_transform_json_to_policy(p_policy_table_io in out nocopy bis_ws.t_policy_tab);

  -- Function to set where unique party ID is stored in database. Returns value from specified table column for requested party.
  function get_party_unique_id(p_unique_id varchar2) return varchar2;

  -- Function to allow object price calculation and populates bis_customer_portfolio view with object price
  -- p_seq_no object seq no
  -- p_agr_line_seq_no agreement line seq no
  function get_object_price(p_seq_no          varchar2,
                            p_agr_line_seq_no varchar2) return integer;

  -- Function returns 1 if UUID should be included in return to SPD.
  function get_portfolio_config(p_table_name varchar2) return integer;

  -- Function returns 1 if product variants risks should be included in return to SIPAS.
  function get_uuid_obj_risk_slave(p_product_line_id varchar2) return integer;

  -- Function to control if set in force should be performed for specified policy and policy line.
  -- If function returns false, BIS will not continue with policy transaction and if this fucntion should have logic
  -- that should be used instead.
  function set_in_force(p_policy_no      policy.policy_no%type,
                        p_policy_line_no policy_line.agr_line_no%type) return boolean;

  -- Function to manipulate Quick-Quote payload before invoking Quick Quote in Core REST_API
  function pre_process_qq_request(p_clob clob) return clob;

  -- Function to enable use of PL first start date in database when calculating premium on existing policies.
  -- The function adds first start date in payload. Function is called before invoking Quick Quote
  function add_date_before_qq_request(p_json_data in clob) return clob;

  -- Function to manipulate price after invoking Quick Quote in Core REST_API
  procedure after_qq_request(p_clob clob);

  -- Function to include object flex fields C150-C180
  procedure add_flexfields_dif_object(p_items_changed varchar2,
                                      p_object        bis_ws.t_object);

  -- Function to include object flex fields C150-C180
  procedure object_flexfields_exec_sql(p_transdata_seqno integer,
                                       p_item_name       varchar2);

  -- Function for policy number retrvieval where backend sales policy should be merged as agreement line
  function get_policy_no_to_merge(p_policy in out bis_ws.t_policy) return number;


  --Function for adding customized policy cancel codes in bis_ws.cancel_policy
  function get_policy_cc(p_policy_no policy.policy_no%type,
                         p_date      date) return number;

  --Function for adding customized agreement line cancel codes in bis_ws.cancel_policy
  function get_policy_line_cc(p_policy_no      policy.policy_no%type,
                              p_policy_line_no agreement_line.agr_line_no%type,
                              p_date           date) return number;

  --Function for adding customized quote cancel codes in bis_ws.cancel_policy
  function get_q_policy_cc(p_policy_no policy.policy_no%type,
                           p_date      date) return number;

  --Function for adding customized quote agreement line cancel codes in bis_ws.cancel_policy
  function get_q_policy_line_cc(p_policy_no      policy.policy_no%type,
                                p_policy_line_no agreement_line.agr_line_no%type,
                                p_date           date) return number;
  /** Labas diena **/
  -- Procedure for adding custom BIS web service call initiation logic
  procedure init(p_user top_user.user_id%type,
                 p_lang top_user.language%type := null);

  -- Procedure for additional checks if policy objects of SPD can be aggregated
  function merge_policies(p_policy_dest      bis_ws.t_policy,
                          p_policy_src       bis_ws.t_policy,
                          p_aggregation_mode varchar2) return boolean;

  -- Procedure for custom error handling
  procedure modify_error_messages(p_policy   in clob,
                                  p_messages in out tab_message);

  -- Procedure is used for additional error handling when pol_transaction fails
  procedure remove_duplicated_errors(p_policy clob);

   -- Procedure is used for modify errors in p0000.get_and_clear_messages
  procedure get_and_clear_messages(p_messages_tab in out bis_ws.t_messages_rec);

  -- This procedure is called after bis_ws.set_inf_force_by_uuid has done it's execution.
  -- @param p_uuids - list of uuids to set in force separated by comma
  -- @param p_successful - flag indicating if set in force was successful. true - successful. false - not successful
  -- @param p_failing_policy_no - failing policy_no if p_successful = false
  procedure after_set_in_force_by_uuid(p_uuids varchar2, p_successful boolean, p_failing_policy_no number default null);

end uf96;
/
create or replace package body uf96 is

  --------------------------------------------------------------------------
  -- Subject    : User functions for Sipas webservice layer
  -- File       : $Release: @releaseVersion@ $
  --              $Id: UF96.pls 100042 2019-01-14 13:52:21Z nte $
  -- Copyright (c) TIA Technology A/S. All rights reserved
  --------------------------------------------------------------------------

  function get_all_product_lines(p_prod_id                 varchar2,
                                 p_product_line_id         varchar2,
                                 p_product_line_version_no number) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 1;
  begin
    z_program('uf96.get_all_product_lines');
    z_trace('p_product_line_id=' || p_product_line_id);

    if p_prod_id in ('MF', 'PF') then
      w_return := 0;
    end if;

    /*
    -- Sample code

    if p_product_line_id = 'ZC' then
      w_return := 0;
    end if;
    return 'ALIO';
    w_return := 0;
    for r in (select 1
                from fw_ppl_comb
               where product_line_id = p_product_line_id
                 and rownum = 1) loop
      w_result := 1;
    end loop;
    */

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_all_product_lines;

  --------------------------------------------------------------------------

  function get_obj_config(p_item_name      varchar2,
                          p_object_type_id varchar2) return integer is
    lk_exclusive_obj_types constant tia_preference.arguments%type := z_site_preference('SPD_EXCLUSIVE_OBJ_TYPE');
    lk_exclude_obj_fields constant tia_preference.arguments%type := z_site_preference('SPD_EXCLUDE_OBJ_FIELDS');
    w_program p0000.program%type := p0000.program;
    w_return  integer := 1;
  begin
    z_program('uf96.get_obj_config');
    z_trace('p_item_name=' || p_item_name);
    z_trace('p_object_type_id=' || p_object_type_id);

    if instr(lk_exclusive_obj_types, p_object_type_id) > 0 then
      w_return := 0;
    end if;

    if instr(lk_exclude_obj_fields, p_item_name) > 0 then
      w_return := 0;
    end if;
    /*
    -- Sample code

    if p_item_name like '%_TEXT%' then
      w_return := 0;
    end if;

    if p_object_type_id = 'FWMO2' then
      w_return := 0;
    end if;

    */

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_obj_config;
  --------------------------------------------------------------------------

  function get_obj_slave_config(p_item_name         varchar2,
                                p_obj_slave_type_id varchar2) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 1;
  begin
    z_program('uf96.get_obj_slave_config');
    z_trace('p_item_name=' || p_item_name);
    z_trace('p_obj_slave_type_id=' || p_obj_slave_type_id);

    /*
    -- Sample code

    if p_item_name like '%_TEXT%' then
      w_return := 0;
    end if;

    if p_obj_slave_type_id = 'ABC02' then
      w_return := 0;
    end if;

    */

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_obj_slave_config;
  --------------------------------------------------------------------------

  function get_lov_config(p_table_name varchar2) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 1;
  begin
    z_program('uf96.get_lov_config');
    z_trace('p_table_name=' || p_table_name);

    /*
    -- Sample code
    if instr(z_site_preference('BIS_XLA_REFERENCES', ','||p_table_name||',') > 0 then
      w_return := 1;
    end if;
    */
    if p_table_name = 'INDUSTRY_CODE' then
      w_return := 0;
    end if;

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_lov_config;
  --------------------------------------------------------------------------
  function get_cancel_code_config(p_line_cancel_code number,
                                  p_policy_status    varchar2) return integer is
    w_program          p0000.program%type := p0000.program;
    w_line_cancel_code integer;
    w_policy_status    varchar2(12);
    w_return           integer;
  begin
    z_program('uf96.get_cancel_code_config');
    z_trace('p_line_cancel_code=' || p_line_cancel_code);

    -- Sample code
    w_line_cancel_code := p_line_cancel_code;
    w_policy_status    := p_policy_status;

    if w_line_cancel_code in (0, 92, 97, 98) then
      w_return := 1;
    elsif w_policy_status in ('QS', 'PS') then
      -- cancel code N/A for status QS and PS
      w_return := 1;
    else
      w_return := 0;
    end if;

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_cancel_code_config;
  --------------------------------------------------------------------------

  function get_portfolio_config(p_table_name varchar2) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 1;
  begin
    z_program('uf96.get_portfolio_config');
    z_trace('p_table_name=' || p_table_name);

    /*
    -- Sample code
    if instr(z_site_preference('BIS_XLA_REFERENCES', ','||p_table_name||',') > 0 then
      w_return := 1;
    end if;
    */

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_portfolio_config;

  --------------------------------------------------------------------------

  function get_object_price(p_seq_no          varchar2,
                            p_agr_line_seq_no varchar2) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 0;
  begin
    z_program('uf96.get_object_price');
    z_trace('p_seq_no=' || p_seq_no);
    z_trace('p_agr_line_seq_no=' || p_agr_line_seq_no);

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_object_price;

  --------------------------------------------------------------------------

  function get_pl_clauses(p_clause_id       number,
                          p_product_line_id varchar2) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 1;
  begin
    z_program('uf96.get_pl_clauses');
    z_trace('p_clause_id=' || p_clause_id);
    z_trace('p_product_line_id=' || p_product_line_id);

    /*
    -- Sample code
    if instr(z_site_preference('CLAUSEIDS', ','||p_clause_id||',') > 0 then
      w_return := 1;
    end if;
    */

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_pl_clauses;
  --------------------------------------------------------------------------

  function get_lov_pv_config(p_product_line_id varchar2) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 1;
  begin
    z_program('uf96.get_lov_pv_config');
    z_trace('p_product_line_id=' || p_product_line_id);

    /*
    -- Sample code
    if instr(z_site_preference('EXAMPLE_NAME', ','||p_product_line_id||',') > 0 then
      w_return := 1;
    end if;
    */
    if p_product_line_id = 'ZSV' then
      w_return := 1;
    end if;

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_lov_pv_config;

  --------------------------------------------------------------------------

  function get_risk_config(p_product_line_id     varchar2,
                           p_product_line_ver_no number,
                           p_object_type_id      varchar2,
                           p_risk_no             number) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 1;
  begin
    z_program('uf96.get_risk_config');
    z_trace('p_product_line_id=' || p_product_line_id);
    z_trace('p_risk_no=' || p_risk_no);

    /*
    -- Sample code

    if p_product_line_id = 'ZC' and p_risk_no = 1 then
      w_return := 0;
    end if;

    */
    w_return := 0;
    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_risk_config;

  --------------------------------------------------------------------------

  procedure before_insert_int_party_obj(w_id_no number) is
    cursor c_get_ip_c01(cp_id_no interested_party.id_no%type) is
      select ip.ip_type
      from   interested_party ip
      where  ip.id_no = cp_id_no;

    w_program p0000.program%type := p0000.program;
    w_ip_c01  c_get_ip_c01%rowtype;
  begin
    z_program('uf96.before_insert_int_party_obj');

    open c_get_ip_c01(w_id_no);
    fetch c_get_ip_c01
      into w_ip_c01;
    close c_get_ip_c01;
    -- ip_type = 20 only when it's Leasing Customer interested party
    if w_ip_c01.ip_type = 20 then
      t1420.rec.c01        := '001';
      t1420.rec.short_desc := 'Leasing Customer';
    end if;

    z_program(w_program);
  exception
    when others then
      z_error_handle;
  end before_insert_int_party_obj;

  --------------------------------------------------------------------------
  procedure before_pol_trans(p_policy_table in out bis_ws.t_policy_tab) is
    -- note: if changing policy structure, especially when action changes between New and Adjust,
    -- then remember to update p_policy_table with changed actions (f.ex. p_policy_table(n).policy_action)
    w_program      p0000.program%type := p0000.program;
    l_policy_table bis_ws.t_policy_tab;
    l_line_count   number;
    l_merged       boolean;
    l_pl_prod_ids  tia_preference.arguments%type := x_get_var(z_site_preference('SNO_PR_CL_PL_PRODUCT_IDS'), 'PL');

    cursor c_get_agent(cp_policy_holder_id policy.policy_holder_id%type) is
      select p.agent_no_1,
             p.com_group_1,
             agent_role_1
      from   policy        p,
             policy_entity pe
      where  p.policy_holder_id = cp_policy_holder_id
      and    pe.policy_no = p.policy_no
      and    instr(l_pl_prod_ids, ',' || pe.prod_id || ',') > 0
      and    suc_seq_no is null
      and    p.cancel_code = 0
      and    p.policy_status = 'P'
      and    p.agent_no_1 is not null
      order  by p.uix_seq_no desc;
    w_get_agent c_get_agent%rowtype;

  begin
    z_program('uf96.before_pol_trans');
	
	/* Milage validation call */
	/* This call should go before any automated changes on the object level */
    bis_ws_custom.milage_val(p_policy_table);

    /* deleting n140 as it is not part of object definition but it holds object price*/
    for w_pidx in p_policy_table.first .. p_policy_table.last
    loop
      /* DSI-1609. When there is no active policy for LOB, but there is a policy for other LOB, thenAgent on the policy should be taken from existing policy for PL lines*/
      if instr(l_pl_prod_ids, ',' || p_policy_table(w_pidx).prod_id || ',') > 0 then
        open c_get_agent(p_policy_table(w_pidx).policy_rec.policy_holder_id);
        fetch c_get_agent
          into w_get_agent;
        close c_get_agent;
        if w_get_agent.agent_no_1 is not null and w_get_agent.agent_role_1 is not null and w_get_agent.com_group_1 is not null then
          p_policy_table(w_pidx).policy_rec.agent_no_1 := w_get_agent.agent_no_1;
          p_policy_table(w_pidx).policy_rec.com_group_1 := w_get_agent.com_group_1;
          p_policy_table(w_pidx).policy_rec.agent_role_1 := w_get_agent.agent_role_1;
          p_policy_table(w_pidx).items_changed := p_policy_table(w_pidx).items_changed || ',agent_no_1,com_group_1,agent_role_1,';
        end if;
      end if;
      for w_lidx in p_policy_table(w_pidx).policy_line_tab.first .. p_policy_table(w_pidx).policy_line_tab.last
      loop
        for w_oidx in p_policy_table(w_pidx).policy_line_tab(w_lidx).policy_object_tab.first .. p_policy_table(w_pidx).policy_line_tab(w_lidx)
                                                                                                .policy_object_tab.last
        loop
          p_policy_table(w_pidx).policy_line_tab(w_lidx).policy_object_tab(w_oidx).items_changed := replace(p_policy_table(w_pidx).policy_line_tab(w_lidx).policy_object_tab(w_oidx).items_changed,
                                                                                                            ',n140,',
                                                                                                            ',');
        end loop;
      end loop;
    end loop;

    z_program(w_program);
  exception
    when others then
      z_error_handle;
  end before_pol_trans;

  --------------------------------------------------------------------------

  procedure after_pol_trans(p_policy_table in out bis_ws.t_policy_tab,
                            p_policy_no    policy.policy_no%type default null) is
    w_program p0000.program%type := p0000.program;
    
    w_agentfirm_id               ds_opportunity_basket.agent_firm_id%type;
    w_basket_no                  ds_opportunity_basket.opportunity_basket_no%type;  
    
    cursor c_get_firm(cp_opp_id ds_opportunity_basket.opportunity_id%type) is
      select owner
        from relation              r,
             agreement_line        al,
             ds_opportunity_basket ob,
             policy                p
       where al.c10 = ob.opportunity_id
         and ob.opportunity_id = cp_opp_id
         and al.policy_no = p.policy_no
         and p.agent_no_1 = r.member
         and p.newest = 'Y'
         and p.suc_seq_no is null
         and al.newest = 'Y'
         and al.suc_seq_no is null
         and r.relation_type in ('3', '4');
         
    cursor c_get_basket_no(cp_policy_no policy.policy_no%type) is
       select opportunity_basket_no
         from  ds_opportunity_basket ob,
               agreement_line al 
         where al.policy_no = cp_policy_no
         and   al.newest = 'Y'
         and   al.suc_seq_no is null
         and   al.c10 = ob.opportunity_id;    
  begin
    z_program('uf96.after_pol_trans');
    if p0000.client_type = 'SPD' then
      open c_get_basket_no(p_policy_no);
      fetch c_get_basket_no
        into w_basket_no;
      close c_get_basket_no;
      if w_basket_no is not null then
        t4207.sel(P_opportunity_basket_no => w_basket_no);
        
        open c_get_firm(t4207.rec.opportunity_id);
        fetch c_get_firm
          into w_agentfirm_id;
        close c_get_firm;
        
        --DSI-2222: if agent is empty on policy or firm not found prevent update
        if w_agentfirm_id is not null then
          t4207.rec.agent_firm_id := w_agentfirm_id;
          t4207.items_changed := t4207.items_changed || ',agent_firm_id,';
          t4207.upd;
        end if;
        z_trace('agent firm on basket:'||t4207.rec.agent_firm_id||' :w_agentfirm_id='||w_agentfirm_id);
      end if;
    end if;
    z_program(w_program);
  exception
    when others then
      z_error_handle;
  end after_pol_trans;

  --------------------------------------------------------------------------
  procedure post_transform_json_to_policy(p_policy_table_io in out nocopy bis_ws.t_policy_tab) is
    w_old_prog p0000.program%type := p0000.program;
    w_prog     p0000.program%type := 'uf96.post_transform_json_to_policy';
  begin
    z_program(w_prog);
    z_trace('Started!');

    --p_policy_table_io := ...

    z_trace('Done!');
    z_program(w_old_prog);
  end post_transform_json_to_policy;
  --------------------------------------------------------------------------

  function get_uuid_obj_risk_slave(p_product_line_id varchar2) return integer is
    w_program p0000.program%type := p0000.program;
    w_return  integer := 0;

  begin
    z_program('uf96.get_risk_slave_uuid');
    z_trace('p_product_line_id=' || p_product_line_id);

    /*
    -- Sample code
    if instr(z_site_preference('BIS_NAMING_EXAMPLE', ','||p_product_line_id||',') > 0 then
      w_return := 1;
    end if;

    */

    z_program(w_program);

    return w_return;
  exception
    when others then
      z_error_handle;
  end get_uuid_obj_risk_slave;

  --------------------------------------------------------------------------

  function get_party_unique_id(p_unique_id varchar2) return varchar2 is
    w_program p0000.program%type := p0000.program;
    w_id_no   varchar2(2000);
    w_counter number;

    cursor c_party_unique_id is
      select id_no
      from   name
      where  civil_reg_code = p_unique_id; --    change to the table and column where unique ID is saved

  begin
    z_program('uf96.get_party_unique_id');
    w_counter := 0;

    for c1 in c_party_unique_id
    loop
      w_id_no   := c1.id_no;
      w_counter := w_counter + 1;
      if w_counter = 2 then
        z_error(p_message => 'Get party by unique id returns more than one record', p_message_id => 'BIS-WS-PAR-0001');
      end if;
    end loop;

    if w_id_no is not null then
      z_program(w_program);
      return w_id_no;
    end if;

    return null;
  exception
    when others then
      z_error_handle;
  end get_party_unique_id;

  --------------------------------------------------------------------------

  function set_in_force(p_policy_no      policy.policy_no%type,
                        p_policy_line_no policy_line.agr_line_no%type) return boolean is
    w_program p0000.program%type := p0000.program;
    w_return  boolean := true;

  begin
    z_program('uf96.set_in_force');

    z_program(w_program);
    return w_return;
  exception
    when others then
      z_error_handle;
  end set_in_force;

  --------------------------------------------------------------------------

  -- Function to manipulate Quick-Quote payload before invoking Quick Quote in Core REST_API
  function pre_process_qq_request(p_clob clob) return clob is
  begin
    -- KAL Temporary Solution for Handling Large Object Actions SUP-1390
    return bis_ws_custom.modify_qq_payload(p_clob);
  end;

  --------------------------------------------------------------------------
  function add_date_before_qq_request(p_json_data in clob) return clob is


    cursor gc_single_actions(p_clob clob) is
      select *
      from   json_table(p_clob,
                        '$' columns(trans_type varchar2(20) path '$.type',
                                context_name varchar2(10) path '$.contextName',
                                nested path '$.actions[*]'
                              columns(action varchar2(60) path '$.action',
                                      deleteYn varchar2(1) path '$.deleteYn'))) js
      where  js.action in ('New Policy Line');

    cursor find_first_start_date(p_agr_line_no number) is
      select first_start_date
      from   agreement_line
      where  agr_line_no = p_agr_line_no
      and    suc_seq_no is null;

    cursor c_json_content_non_object(p_clob clob) is
      select *
      from   json_table(p_clob,
                        '$' columns(trans_type varchar2(20) path '$.type',
                                context_name varchar2(10) path '$.contextName',
                                nested path '$.actions[*]'
                                columns(action varchar2(60) path '$.action',
                                      deleteYn varchar2(1) path '$.deleteYn',
                                        value_array varchar2(4000) format json with conditional wrapper path '$.values[*]')));

    w_record_count     number := 0;
    w_action_count     number := 0;
    w_clob             clob;
    w_object_added     number := 0;
    l_agr_line_no      number := 0;
    l_first_start_date varchar2(50);

    procedure add_line(p_line varchar2) is
    begin
      dbms_lob.writeappend(w_clob, length(p_line), p_line);
    end;

  begin

    dbms_lob.createtemporary(w_clob, true);
    for r_actions in gc_single_actions(p_json_data)
    loop
      for r in (select js.name,
                       js.value
                from   json_table(p_json_data,
                                  '$' columns(trans_type varchar2(20) path '$.type',
                                          context_name varchar2(10) path '$.contextName',
                                          nested path '$.actions[*]'
                                          columns(action varchar2(60) path '$.action',
                                           deleteYn varchar2(1) path '$.deleteYn',
                                           nested path '$.values[*]'
                                           columns(name varchar2(100) path '$.name',
                                                          value varchar2(4000) path '$.value',
                                                          type varchar2(10) path '$.type')))) js
                where  js.name = 'agrLineNo')
      loop
        l_agr_line_no := r.value;

        for r1 in find_first_start_date(l_agr_line_no)
        loop
          l_first_start_date := to_char(to_date(r1.first_start_date), 'YYYY-MM-DD') || 'T00:00:00.000';
        end loop;
      end loop;
    end loop;

    if l_agr_line_no = 0 then
      return p_json_data;
    end if;

    for r_json_node in c_json_content_non_object(p_json_data)
    loop
      if w_record_count = 0 then
        -- Add Payload Type and Context Name in the start
        add_line('{"type":"' || r_json_node.trans_type || '",');
        if r_json_node.context_name is not null then
          add_line('{"contextName":"' || r_json_node.context_name || '",');
        end if;
        add_line('"actions":[');
      end if;

      w_record_count := w_record_count + 1;
      --Add entire non-object Node at once
      if r_json_node.action in ('New Policy', 'New Risk', 'New Sub-Object', 'New Object') then
        if w_action_count > 0 then
          add_line(',');
        end if;
        add_line('{"action":"' || r_json_node.action || '","values":' || r_json_node.value_array || '}');
        w_action_count := w_action_count + 1;
      else
        if r_json_node.action in ('New Policy Line') then
          if w_object_added = 0 then
            w_object_added := 1;
            if w_action_count > 0 then
              add_line(',');
            end if;
            add_line('{"action":"' || r_json_node.action || '","values":' ||
                     substr(r_json_node.value_array, 0, length(r_json_node.value_array) - 1) || ',{"name":"firstStartDate","value":"' ||
                     l_first_start_date || '"}]}');
            w_action_count := w_action_count + 1;
          end if;
        end if;
      end if;
    end loop; -- end loop through actions
    add_line(']}');

    return w_clob;
  end add_date_before_qq_request;

  --------------------------------------------------------------------------
  -- Function to manipulate price after invoking Quick Quote in Core REST_API
  procedure after_qq_request(p_clob clob) is
    w_program p0000.program%type := p0000.program;
  begin
    z_program('uf96.after_qq_request');
    null;
    z_program(w_program);
  exception
    when others then
      z_error_handle;
  end after_qq_request;

  --------------------------------------------------------------------------
  -- Function to include object flex fields C150-C180
  procedure add_flexfields_dif_object(p_items_changed varchar2,
                                      p_object        bis_ws.t_object) is
    w_program p0000.program%type := p0000.program;
  begin

    --   if instr(p_items_changed,'c150') > 0 then t1104.rec.c150 := p_object.policy_object.c150; end if;
    --   if instr(p_items_changed,'c151') > 0 then t1104.rec.c151 := p_object.policy_object.c151; end if;
    --   if instr(p_items_changed,'c152') > 0 then t1104.rec.c152 := p_object.policy_object.c152; end if;
    --   if instr(p_items_changed,'c153') > 0 then t1104.rec.c153 := p_object.policy_object.c153; end if;
    --   if instr(p_items_changed,'c154') > 0 then t1104.rec.c154 := p_object.policy_object.c154; end if;
    --   if instr(p_items_changed,'c155') > 0 then t1104.rec.c155 := p_object.policy_object.c155; end if;
    --   if instr(p_items_changed,'c156') > 0 then t1104.rec.c156 := p_object.policy_object.c156; end if;
    --   if instr(p_items_changed,'c157') > 0 then t1104.rec.c157 := p_object.policy_object.c157; end if;
    --   if instr(p_items_changed,'c158') > 0 then t1104.rec.c158 := p_object.policy_object.c158; end if;
    --   if instr(p_items_changed,'c159') > 0 then t1104.rec.c159 := p_object.policy_object.c159; end if;
    --   if instr(p_items_changed,'c160') > 0 then t1104.rec.c160 := p_object.policy_object.c160; end if;
    --   if instr(p_items_changed,'c161') > 0 then t1104.rec.c161 := p_object.policy_object.c161; end if;
    --   if instr(p_items_changed,'c162') > 0 then t1104.rec.c162 := p_object.policy_object.c162; end if;
    --   if instr(p_items_changed,'c163') > 0 then t1104.rec.c163 := p_object.policy_object.c163; end if;
    --   if instr(p_items_changed,'c164') > 0 then t1104.rec.c164 := p_object.policy_object.c164; end if;
    --   if instr(p_items_changed,'c165') > 0 then t1104.rec.c165 := p_object.policy_object.c165; end if;
    --   if instr(p_items_changed,'c166') > 0 then t1104.rec.c166 := p_object.policy_object.c166; end if;
    --   if instr(p_items_changed,'c167') > 0 then t1104.rec.c167 := p_object.policy_object.c167; end if;
    --   if instr(p_items_changed,'c168') > 0 then t1104.rec.c168 := p_object.policy_object.c168; end if;
    --   if instr(p_items_changed,'c169') > 0 then t1104.rec.c169 := p_object.policy_object.c169; end if;
    --   if instr(p_items_changed,'c170') > 0 then t1104.rec.c170 := p_object.policy_object.c170; end if;
    --   if instr(p_items_changed,'c171') > 0 then t1104.rec.c171 := p_object.policy_object.c171; end if;
    --   if instr(p_items_changed,'c172') > 0 then t1104.rec.c172 := p_object.policy_object.c172; end if;
    --   if instr(p_items_changed,'c173') > 0 then t1104.rec.c173 := p_object.policy_object.c173; end if;
    --   if instr(p_items_changed,'c174') > 0 then t1104.rec.c174 := p_object.policy_object.c174; end if;
    --   if instr(p_items_changed,'c175') > 0 then t1104.rec.c175 := p_object.policy_object.c175; end if;
    --   if instr(p_items_changed,'c176') > 0 then t1104.rec.c176 := p_object.policy_object.c176; end if;
    --   if instr(p_items_changed,'c177') > 0 then t1104.rec.c177 := p_object.policy_object.c177; end if;
    --   if instr(p_items_changed,'c178') > 0 then t1104.rec.c178 := p_object.policy_object.c178; end if;
    --   if instr(p_items_changed,'c179') > 0 then t1104.rec.c179 := p_object.policy_object.c179; end if;
    --   if instr(p_items_changed,'c180') > 0 then t1104.rec.c180 := p_object.policy_object.c180; end if;

    z_program('uf96.add_flexfields_obj_dif');
    null;
    z_program(w_program);
  exception
    when others then
      z_error_handle;
  end add_flexfields_dif_object;
  /** Bobule nusipirko dvirati **/
  --------------------------------------------------------------------------
  -- Function to include object flex fields C150-C180
  procedure object_flexfields_exec_sql(p_transdata_seqno integer,
                                       p_item_name       varchar2) is
    w_program p0000.program%type := p0000.program;
  begin

    /*
          if p_item_name = 'c150' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c150); end if;
          if p_item_name = 'c151' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c151); end if;
          if p_item_name = 'c152' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c152); end if;
          if p_item_name = 'c153' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c153); end if;
          if p_item_name = 'c154' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c154); end if;
          if p_item_name = 'c155' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c155); end if;
          if p_item_name = 'c156' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c156); end if;
          if p_item_name = 'c157' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c157); end if;
          if p_item_name = 'c158' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c158); end if;
          if p_item_name = 'c159' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c159); end if;
          if p_item_name = 'c160' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c160); end if;
          if p_item_name = 'c161' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c161); end if;
          if p_item_name = 'c162' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c162); end if;
          if p_item_name = 'c163' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c163); end if;
          if p_item_name = 'c164' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c164); end if;
          if p_item_name = 'c165' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c165); end if;
          if p_item_name = 'c166' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c166); end if;
          if p_item_name = 'c167' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c167); end if;
          if p_item_name = 'c168' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c168); end if;
          if p_item_name = 'c169' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c169); end if;
          if p_item_name = 'c170' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c170); end if;
          if p_item_name = 'c171' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c171); end if;
          if p_item_name = 'c172' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c172); end if;
          if p_item_name = 'c173' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c173); end if;
          if p_item_name = 'c174' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c174); end if;
          if p_item_name = 'c175' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c175); end if;
          if p_item_name = 'c176' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c176); end if;
          if p_item_name = 'c177' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c177); end if;
          if p_item_name = 'c178' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c178); end if;
          if p_item_name = 'c179' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c179); end if;
          if p_item_name = 'c180' then p1140.set_property(p_transdata_seqno,p_item_name,t1104.rec.c180); end if;
    */

    z_program('uf96.object_flexfields_exec_sql');
    null;
    z_program(w_program);
  exception
    when others then
      z_error_handle;
  end object_flexfields_exec_sql;

  --------------------------------------------------------------------------

  function get_policy_no_to_merge(p_policy in out bis_ws.t_policy) return number is


    w_days_to_merge number := to_number(z_site_preference('SPD_DAYS_TO_MERGE'));
    k_pl_prod_ids   tia_preference.arguments%type := x_get_var(z_site_preference('SNO_PR_CL_PL_PRODUCT_IDS'), 'PL');
    k_cl_prod_ids   tia_preference.arguments%type := x_get_var(z_site_preference('SNO_PR_CL_PL_PRODUCT_IDS'), 'CL');

    /* cursor returns policy number by rules and priorities
       Rules:
         1. Return only active not cancelled policy versions
         2. Return only quotes, policies and suspended quotes
         3. Return only these quotes, policies and suspended quotes which are no older than 30 days and not in the future compared to the merger policy
         4. Return only these quotes, policies and suspended quotes which have the same affinity number camper to the merger policy
         5. Return only these quotes, policies and suspended quotes which have the same agent/broker/ house as merger policy
         6. If policy belongs to PL lines (M2, P3), return only these quotes and policies which have sales channel (10,20,40)
       Priorities:
         1. Quotes over policies
         2. Cancel code 0 over everything else
         3. The same basket over every else basket
         4. Policies with the lowest cover start date span compared to the merger policy
    */
    cursor c_policy(cp_policy        bis_ws.t_policy,
                    cp_relation_type varchar2) is
      select policy_no,
             policy_status
      from   (select p.policy_no,
                     p.policy_status,
                     nvl(cp_policy.policy_rec.cover_start_date, sysdate) - p.cover_start_date as days_from_cover_start,
                     decode(a.c10, cp_policy.policy_line_tab(1).policy_line.c10, a.c10, null) norm_basket_id,
                     decode(p.policy_status, 'Q', 1, 'P', 2, 'QS', 3, null) policy_status_priority,
                     p.cancel_code
              from   agreement_line_in_policy_view2 aip,
                     policy                         p,
                     policy_entity                  e,
                     agreement_line                 a
              where  aip.policy_seq_no = p.policy_seq_no
              and    aip.agr_line_seq_no = a.agr_line_seq_no
              and    p.policy_no = e.policy_no
              and    p.cancel_code in (0, 92)
              and    p.suc_seq_no is null --added this to get the latest policy versions and not all of them
              and    p.policy_holder_id = cp_policy.policy_rec.policy_holder_id
              and    e.prod_id = cp_policy.prod_id
              and    nvl(a.affinity_no, -1) = nvl(cp_policy.policy_line_tab(1).policy_line.affinity_no, -1)
              and    ((p.agent_no_1 in (select r.member
                                        from   relation r
                                        where  instr(cp_relation_type, ',' || relation_type || ',') > 0
                                        and    r.owner in (select owner
                                                           from   relation
                                                           where  instr(cp_relation_type, ',' || relation_type || ',') > 0
                                                           and    member = cp_policy.policy_rec.agent_no_1)
                                        and    trunc(sysdate) between r.start_date and nvl(r.end_date, to_date('3000', 'YYYY'))
                                        and    r.newest = 'Y')) or (p.agent_no_1 is null and cp_policy.policy_rec.agent_no_1 is null))
              and    ((instr(k_pl_prod_ids, ',' || e.prod_id || ',') > 0 and
                    (cp_policy.policy_line_tab(1).policy_line.n01 in (10, 20, 40) or p.agent_no_1 = cp_policy.policy_rec.agent_no_1 or
                     (p.agent_no_1 is null and cp_policy.policy_rec.agent_no_1 is null))) or
                    (instr(k_cl_prod_ids, ',' || e.prod_id || ',') > 0 -- moved some CL logic here, to get correct PL functionality
                    and (p.agent_no_1 = cp_policy.policy_rec.agent_no_1 and p.agent_role_1 = cp_policy.policy_rec.agent_role_1))))
      where  days_from_cover_start >= 0
      and    days_from_cover_start < nvl(w_days_to_merge, 0)
      and    policy_status_priority is not null
      order  by policy_status_priority asc,
                cancel_code            asc,
                norm_basket_id         nulls last,
                days_from_cover_start  asc;

    w_program       p0000.program%type := p0000.program;
    w_policy        number;
    w_policy_status varchar2(2);
    -- DSI-886 merge policy to broker too (or any other type of relation provided in site preference)
    k_relation_type constant tia_preference.arguments%type := z_site_preference('SPD_SC_REL_TYPES');
    k_merge_yn      constant varchar2(20) := z_site_preference(p_varname => 'SPD_MERGE_POLICY');
  begin
    z_program('uf96.get_policy_no_to_merge');

    -- 2020-04-17 check whether Policy merge functionality is turned on
    if nvl(k_merge_yn, 'N') = 'Y' then
      -- 2020-03-03 DSI-35 merge policies if meet grouping criteria
      /*check if policy is not locked by another user*/
      for r in c_policy(p_policy, k_relation_type)
      loop
        if p1100.find_transaction_status(p_policy_no => r.policy_no) in (0, 1, 2) then
          w_policy        := r.policy_no;
          w_policy_status := r.policy_status;
      exit;
        end if;
    end loop;

      /* do not override existing policy data by clearing out merger policy data*/
      if w_policy is not null then
        --dge 07-05-2020 added c11- CRM id must be also changed on merging policy
        p_policy.items_changed := ',c11,cover_start_date,';
        if w_policy_status = 'P' then
          p_policy.policy_line_tab(1).policy_line.cancel_code := 97;
          p_policy.policy_line_tab(1).items_changed := p_policy.policy_line_tab(1).items_changed || ',cancel_code,';
        end if;
      end if;
    end if;
    z_program(w_program);
    return w_policy;
  end get_policy_no_to_merge;

  --------------------------------------------------------------------------
  function get_policy_cc(p_policy_no policy.policy_no%type,
                         p_date      date) return number is
    w_program p0000.program%type := p0000.program;
    w_return  policy.cancel_code%type;
  begin
    z_program('uf96.get_policy_cc');

    z_program(w_program);

    return w_return;
  end get_policy_cc;

  --------------------------------------------------------------------------
  function get_policy_line_cc(p_policy_no      policy.policy_no%type,
                              p_policy_line_no agreement_line.agr_line_no%type,
                              p_date           date) return number is
    w_program p0000.program%type := p0000.program;
    w_return  agreement_line.cancel_code%type;
  begin
    z_program('uf96.get_policy_line_cc');

    z_program(w_program);

    return w_return;
  end get_policy_line_cc;

 --------------------------------------------------------------------------
  function get_q_policy_cc(p_policy_no policy.policy_no%type,
                           p_date      date) return number is
    w_program p0000.program%type := p0000.program;
    w_return  policy.cancel_code%type;
  begin
    z_program('uf96.get_q_policy_cc');

    z_program(w_program);

    return w_return;
  end get_q_policy_cc;

  --------------------------------------------------------------------------
  function get_q_policy_line_cc(p_policy_no      policy.policy_no%type,
                                p_policy_line_no agreement_line.agr_line_no%type,
                                p_date           date) return number is
    w_program p0000.program%type := p0000.program;
    w_return  agreement_line.cancel_code%type;
  begin
    z_program('uf96.get_q_policy_line_cc');

    z_program(w_program);

    return w_return;
  end get_q_policy_line_cc;

  --------------------------------------------------------------------------
  procedure init(p_user top_user.user_id%type,
                 p_lang top_user.language%type := null) is
    w_program p0000.program%type := p0000.program;
  begin
    z_program('uf96.init');

    -- 2019-12-03
    p0000.client_type := 'SPD';

    -- DSI-639 clearing global variables.
    p1100.clr();
    psys.structure_key            := null;
    psys.structure_list_ver       := null;
    psys.policy_no                := null;
    psys.policy_seq_no            := null;
    psys.policy_rowid             := null;
    psys.expiry_code              := null;
    psys.policy_disc_pct          := null;
    psys.customer_no              := null;
    psys.policy_start_date        := null;
    psys.section_no               := null;
    psys.policy_line_no           := null;
    psys.policy_line_seq_no       := null;
    psys.agr_line_seq_no          := null;
    psys.policy_line_rowid        := null;
    psys.first_start_date         := null;
    psys.year_start_date          := null;
    psys.policy_year              := null;
    psys.start_date               := null;
    psys.end_date                 := null;
    psys.cancel_code              := null;
    psys.policy_line_disc_pct     := null;
    psys.status                   := null;
    psys.trans_code               := null;
    psys.userid                   := null;
    psys.trans_id                 := null;
    psys.timestamp                := null;
    psys.temporary_insurance      := null;
    psys.center_code              := null;
    psys.payment_frequency        := null;
    psys.agreement_status         := null;
    psys.renewal_date             := null;
    psys.currency_code            := null;
    psys.commission_pct           := null;
    psys.affinity_no              := null;
    psys.policy_line_rec          := null;
    psys.policy_rec               := null;
    psys.policy_entity_rec        := null;
    psys.force_newest_version     := null;
    psys.product_id               := null;
    psys.product_line_id          := null;
    psys.product_line_ver_no      := null;
    psys.flat_premium             := null;
    psys.tariff_type_list_ver     := null;
    psys.risk_split_fixed         := null;
    psys.risk_split_ver           := null;
    psys.tariff_calc_ver          := null;
    psys.ren_rule_ver             := null;
    psys.cover_check_ver          := null;
    psys.endorsement_list_ver     := null;
    psys.obj_type_list_ver        := null;
    psys.clause_list_ver          := null;
    psys.main_product_line_id     := null;
    psys.main_product_line_ver_no := null;
    psys.ui_config_list_ver       := null;
    psys.ui_config_name           := null;
    psys.validation_list_ver      := null;
    psys.program_seq_no           := null;
    psys.contract_seq_no          := null;
    psys.reinsurer_id_no          := null;
    psys.broker_id                := null;
    psys.physical_contract        := null;
    psys.contract_level           := null;
    psys.contract_layer           := null;
    psys.program_currency_code    := null;
    psys.accumulation_seq_no      := null;
    psys.obj_rowid                := null;
    psys.obj_newest               := null;
    psys.obj_start_date           := null;
    psys.obj_seq_no               := null;
    psys.risk_excess              := null;
    psys.risk_sum                 := null;
    psys.risk_discount            := null;
    psys.risk_terms               := null;
    psys.risk_flex1               := null;
    psys.risk_flex2               := null;
    psys.risk_flex3               := null;
    psys.risk_flex4               := null;
    psys.obj_risk_slave_rec       := null;
    psys.object_slave_rec         := null;
    psys.result1                  := null;
    psys.result2                  := null;
    psys.result3                  := null;
    psys.result4                  := null;
    psys.result5                  := null;
    psys.result6                  := null;
    psys.special_terms            := null;
    psys.special_terms1           := null;
    psys.special_terms2           := null;
    psys.special_terms3           := null;
    psys.special_terms4           := null;
    psys.special_terms5           := null;
    psys.special_terms6           := null;
    psys.special_terms7           := null;
    psys.special_terms8           := null;
    psys.special_terms9           := null;
    psys.special_terms10          := null;
    psys.dim01                    := null;
    psys.dim02                    := null;
    psys.dim03                    := null;
    psys.dim04                    := null;
    psys.dim05                    := null;
    psys.dim06                    := null;
    psys.dim07                    := null;
    psys.dim08                    := null;
    psys.dim09                    := null;
    psys.dim10                    := null;
    psys.price                    := null;
    psys.price_paid               := null;
    psys.flat_price               := null;
    psys.flat_price_paid          := null;
    psys.risk01                   := null;
    psys.risk02                   := null;
    psys.risk03                   := null;
    psys.risk04                   := null;
    psys.risk05                   := null;
    psys.risk06                   := null;
    psys.risk07                   := null;
    psys.risk08                   := null;
    psys.risk09                   := null;
    psys.risk10                   := null;
    psys.risk11                   := null;
    psys.risk12                   := null;
    psys.risk13                   := null;
    psys.risk14                   := null;
    psys.risk15                   := null;
    psys.risk16                   := null;
    psys.risk17                   := null;
    psys.risk18                   := null;
    psys.risk19                   := null;
    psys.risk20                   := null;
    psys.risk30                   := null;
    psys.risk31                   := null;
    psys.risk32                   := null;
    psys.risk33                   := null;
    psys.risk34                   := null;
    psys.risk35                   := null;
    psys.risk36                   := null;
    psys.risk37                   := null;
    psys.risk38                   := null;
    psys.risk39                   := null;
    psys.risk40                   := null;
    psys.risk41                   := null;
    psys.risk42                   := null;
    psys.risk43                   := null;
    psys.risk44                   := null;
    psys.risk45                   := null;
    psys.risk46                   := null;
    psys.risk47                   := null;
    psys.risk48                   := null;
    psys.risk49                   := null;
    psys.risk50                   := null;
    psys.risk.delete();
    psys.risk_temp_array.delete();
    psys.stamp_duty                    := null;
    psys.stamp_duty_adjustment         := null;
    psys.stamp_duty_reset              := null;
    psys.stamp_duty_is_paid_by_company := null;
    psys.transaction_cost              := null;
    psys.product_tax1                  := null;
    psys.product_tax2                  := null;
    psys.product_tax3                  := null;
    psys.product_tax4                  := null;
    psys.product_tax5                  := null;
    psys.product_tax6                  := null;
    psys.product_tax7                  := null;
    psys.commission_amt                := null;
    psys.adjust_refund                 := null;
    psys.adjust_refund_end_date        := null;
    psys.covering_risks                := null;
    psys.event_type                    := null;
    psys.cause_type                    := null;
    psys.subcause_type                 := null;
    psys.risk_no                       := null;
    psys.subrisk_no                    := null;
    psys.cover_risk.delete();
    psys.com_trans_id            := null;
    psys.com_trans_code          := null;
    psys.com_trans_date          := null;
    psys.com_start_date          := null;
    psys.com_end_date            := null;
    psys.com_agent_change_date   := null;
    psys.com_change_on           := null;
    psys.com_collected_premium   := null;
    psys.com_written_premium     := null;
    psys.com_12_month_premium    := null;
    psys.com_pre_agr_line_seq_no := null;
    psys.com_agent_row           := null;
    psys.com_agent_no            := null;
    psys.com_group               := null;
    psys.com_category            := null;
    psys.com_agent_role          := null;
    psys.com_percentage_of_com   := null;
    psys.com_product_group       := null;
    psys.com_algorithm           := null;
    psys.com_algorithm_version   := null;
    psys.com_period_count        := null;
    psys.com_item_rec            := null;
    psys.com_risk_trans_code.delete();
    psys.com_collected_risk_premium.delete();
    psys.com_written_risk_premium.delete();
    psys.com_12_month_risk_premium.delete();
    psys.com_agent_no_array.delete();
    psys.com_group_array.delete();
    psys.com_category_array.delete();
    psys.com_agent_role_array.delete();
    psys.com_percentage_of_com_array.delete();
    psys.com_flat_amt.delete();
    psys.com_amt.delete();
    psys.com_premium_amt.delete();
    psys.com_collection_group.delete();
    psys.com_rule_no                := null;
    psys.com_rule_version           := null;
    psys.com_agent_group            := null;
    psys.com_agent_type             := null;
    psys.com_collect_prem_amt       := null;
    psys.com_written_prem_amt       := null;
    psys.com_12_month_prem_amt      := null;
    psys.com_calculated_amt         := null;
    psys.com_used_pct               := null;
    psys.ptr_rec                    := null;
    psys.transaction_request        := null;
    psys.ptr_status                 := null;
    psys.renewal_rule_mode          := null;
    psys.upgrade_tariff_if_possible := null;
    psys.message                    := null;
    psys.window_message             := null;
    psys.object_no                  := null;
    psys.use_in_memory              := null;

    -- Sample code
    /*
    p0000.ui_type := nvl(p0000.ui_type, p0000.ui_type_tia_adf);*/

    --ZLI 2019-06-05 auto-validate underlying system generated types
    declare
      v_v varchar2(2);
    begin
      select v into v_v from (select '2' v from dual union all select '1' v from table(rs_policy_transaction.get_qq_policy)) where v =2;
      select v into v_v from (select '2' v from dual union all select '1' v from table(rs_policy_transaction.get_qq_policy_line)) where v =2;
      select v into v_v from (select '2' v from dual union all select '1' v from table(rs_policy_transaction.get_qq_object)) where v =2;
      select v into v_v from (select '2' v from dual union all select '1' v from table(rs_policy_transaction.get_qq_object_slave)) where v =2;
      select v into v_v from (select '2' v from dual union all select '1' v from table(rs_policy_transaction.get_qq_risk)) where v =2;
      select v into v_v from (select '2' v from dual union all select '1' v from table(rs_policy_transaction.get_qq_risk_split)) where v =2;
    end;

    z_program(w_program);
  end init;

  --------------------------------------------------------------------------
  function merge_policies(p_policy_dest      bis_ws.t_policy,
                          p_policy_src       bis_ws.t_policy,
                          p_aggregation_mode varchar2) return boolean is
    w_program p0000.program%type := p0000.program;
    w_return  boolean := true;
  begin
    z_program('uf96.merge_policies');

    -- Sample code
    /*
    if p_policy_src.prod_id = 'ZC' then
      if p_policy_src.policy_rec.agent_no_1 = p_policy_dest.policy_rec.agent_no_1 then
        w_return := true;
      else
        w_return := true;
      end if;
    end if;*/

    z_program(w_program);

    return w_return;
  end merge_policies;

--------------------------------------------------------------------------
  procedure modify_error_messages(p_policy   in clob,
                                  p_messages in out nocopy tab_message) is
    w_program p0000.program%type := p0000.program;
  begin
    z_program('uf96.modify_error_messages');

    -- Sample code
    /*
    for i in p_messages.first .. p_messages.last loop
      if p_messages(i).message_id = '10001' then
        p_messages(i).message_text := p_messages(i).message_text || ': test';
      end if;
    end loop;
    */

    z_program(w_program);
  end modify_error_messages;

  --------------------------------------------------------------------------
  procedure remove_duplicated_errors(p_policy clob) is
  begin
    -- 2020-10-02 RMI, DSI-1317 Added error message duplicate preventing
    for i in (select distinct message_text,
                              message_type,
                              entity_name,
                              entity_id,
                              item_name,
                              message_id
              from   table(p0000.get_and_clear_messages))
    loop
      p0000.add_message(p_message_type => i.message_type,
                        p_message_text => i.message_text,
                        p_entity_name  => i.entity_name,
                        p_entity_id    => i.entity_id,
                        p_item_name    => i.item_name,
                        p_message_id   => i.message_id);
    end loop;

  end remove_duplicated_errors;

  --------------------------------------------------------------------------
  procedure get_and_clear_messages(p_messages_tab in out bis_ws.t_messages_rec) is
    w_program p0000.program%type := p0000.program;
  begin
    z_program('uf96.get_and_clear_messages');

    -- Sample code
    /*
       p_messages_tab.text := 'Agent error';
    */

    z_program(w_program);
  end get_and_clear_messages;

  -- This procedure is called after bis_ws.set_inf_force_by_uuid has done it's execution.
  -- @param p_uuids - list of uuids to set in force separated by comma
  -- @param p_successful - flag indicating if set in force was successful. true - successful. false - not successful
  -- @param p_failing_policy_no - failing policy_no if p_successful = false
  procedure after_set_in_force_by_uuid(p_uuids varchar2, p_successful boolean, p_failing_policy_no number default null) is
    w_program p0000.program%type := p0000.program;
  begin
    z_program('uf96.modify_error_messages');

    /*
     --Custom code
    */
    z_program(w_program);
  end after_set_in_force_by_uuid;
end uf96;
/
